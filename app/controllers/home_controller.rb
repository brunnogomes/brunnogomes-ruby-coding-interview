class HomeController < ApplicationController
  def index
    @tweets = Tweet.includes(user: :company).limited_per_user.order("created_at DESC").limit(20)
  end
end
