class Tweet < ApplicationRecord
  belongs_to :user

  scope :by_user, ->(user) { where(user: user) }
  scope :limited_per_user, -> { group(:user_id).select('*, max(id)') }

  validates :body, length: { maximum: 180 }
  validate :vaidate_unique_per_day

  private

  def vaidate_unique_per_day
    return unless self.class.by_user(self.user)
                            .where(created_at: Time.current.beginning_of_day..Time.current.end_of_day)
                            .where(body: self.body).exists?

    errors.add(:created_at, 'You have already tweeted today!')
  end
end
