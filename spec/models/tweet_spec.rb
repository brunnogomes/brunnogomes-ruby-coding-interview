require 'rails_helper'

RSpec.describe Tweet, type: :model do
  describe 'validations' do
    context 'with the same user' do
      let(:user) { create(:user) }
      let(:tweet) { create(:tweet) }
      let(:user) { tweet.user }

      subject { user.tweets.build(body: tweet.body) }

      it 'should now allow repeated tweets in the same day' do
        expect(subject.valid?).to be_falsey
      end

    end

    context 'length' do
      let(:user) { create(:user) }
      let(:valid_body) { 'something something' }
      let(:invalid_body) { 'something something' * 200 }

      it 'should allow tweets within 180 chars' do
        tweet = user.tweets.build(body: valid_body)
        expect(tweet.valid?).to be_truthy
      end

      it 'should not allow tweets with more than 180 chars' do
        tweet = user.tweets.build(body: invalid_body)
        expect(tweet.valid?).to be_falsey
      end
    end
  end
end
